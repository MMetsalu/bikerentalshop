
class BikeRental:
    """
    Let's make a bike rental system.
    Customers can:
    *) See available bikes in the shop
    Rent bikes on hourly basis - $5 per hour
    Rent bikes on daily basis - $20 per day
    Rent bikes on weekly basis - $60 per week
    Family rental - User gets 30% discount if they rent
    3 - 5 bikes.

    The shop can:
    Issue a bill when the customer returns the bike
    Display available bikes
    Take bike rental requests - verifying if they have
    stock
    """
    amount_of_bikes = 0
    def __init__(self, amount):
        """See meetod läheb käima kui objekt luuakse."""
        self.amount_of_bikes = amount

    def rent_bike(self, customer, hours, amount_of_bikes):
        """Rent a bike."""
        return True

    def return_bike(self, customer, amount_of_bikes):
        """Return some or all bikes."""
        pass