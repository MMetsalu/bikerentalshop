import unittest
from BikeRental import BikeRental
from Customer import Customer


class BikeRentalTest(unittest.TestCase):
    """Tests."""

    def test1(self):
        """Test 1."""

        # 5 on jalgrataste arv, mis algul laenutuses on
        br = BikeRental(5)

        # self.assertEquals(x, y) kontrollib, kas x ja y on võrdsed
        self.assertEquals(br.amount_of_bikes, 5)

    def test2(self):
        """Test 2."""

        # Uus laenutus, millel on 10 jalgratast sees
        br = BikeRental(10)

        # uus klient
        c1 = Customer()

        # rent_bike(customer, number) tagastab True, kui jalgratast saab laenutada
        self.assertEquals(br.rent_bike(c1, 1), True)

        # 12 jalgratast ei saa laenutada, sest meil oli 10
        self.assertEquals(br.rent_bike(c1, 12), False)

        # 5 saab laenutada
        self.assertEquals(br.rent_bike(c1, 5), True)

        # 6 ei saa enam laenutada, sest me oleme 6 välja laenutanud, meil on 4 alles
        self.assertEquals(br.rent_bike(c1, 6), False)

    def test3(self):
        """Test 3."""

        # Uus laenutus, millel on 10 jalgratast sees
        br = BikeRental(12)

        # uus klient
        c1 = Customer()
        c2 = Customer()

        # rent_bike(customer, number) tagastab True, kui jalgratast saab laenutada
        self.assertEquals(br.rent_bike(c1, 1), True)
        self.assertEquals(br.amount_of_bikes, 11)
        self.assertEquals(br.rent_bike(c2, 1), True)
        self.assertEquals(br.amount_of_bikes, 10)


        # 14 jalgratast ei saa laenutada, sest meil oli 12
        self.assertEquals(br.rent_bike(c1, 14), False)
        self.assertEquals(br.rent_bike(c2, 14), False)

        # Mõlemad c1 ja c2 saavad oma ratta tagastada
        self.assertEquals(br.return_bike(c1, 1), True)
        self.assertEquals(br.amount_of_bikes, 11)
        self.assertEquals(br.return_bike(c2, 1), True)
        self.assertEquals(br.amount_of_bikes, 12)

        # Enam ei saa tagastada, sest nad pole rattaid laenutanud
        self.assertEquals(br.return_bike(c1, 1), False)
        self.assertEquals(br.return_bike(c2, 1), False)

        # Laename uuesti
        # rent_bike(customer, number) tagastab True, kui jalgratast saab laenutada
        self.assertEquals(br.rent_bike(c1, 5), True)
        self.assertEquals(br.amount_of_bikes, 7)
        self.assertEquals(br.rent_bike(c2, 5), True)
        self.assertEquals(br.amount_of_bikes, 2)

        # Ja tagastame alguses 2 ratast
        self.assertEquals(br.return_bike(c1, 2), True)
        self.assertEquals(br.amount_of_bikes, 4)
        self.assertEquals(br.return_bike(c2, 2), True)
        self.assertEquals(br.amount_of_bikes, 6)

        # Proovime tagastada rohkem rattaid, see ei tohi toimida!
        self.assertEquals(br.return_bike(c1, 8), False)
        self.assertEquals(br.return_bike(c2, 8), False)
        self.assertEquals(br.amount_of_bikes, 6)

        # Nüüd tagastame õige arvu rattaid
        self.assertEquals(br.return_bike(c1, 3), True)
        self.assertEquals(br.amount_of_bikes, 9)
        self.assertEquals(br.return_bike(c2, 3), True)
        self.assertEquals(br.amount_of_bikes, 12)

    def test4(self):
        """Test 4."""

        br = BikeRental(20)
        c1 = Customer()
        c2 = Customer()
        c3 = Customer()

        # Kui rattaid on vähem kui klient tahab laenutada siis ei saa laenutada.
        self.assertEquals(br.amount_of_bikes, 20)
        self.assertEquals(br.rent_bike(c1, 5), True)
        self.assertEquals(br.amount_of_bikes, 15)
        self.assertEquals(br.rent_bike(c2, 10), True)
        self.assertEquals(br.amount_of_bikes, 5)
        self.assertEquals(br.rent_bike(c1, 7), False)
        self.assertEquals(br.amount_of_bikes, 5)

